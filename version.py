
import numpy as np

from daily_run_main import get_column_names_by_table, table_exists


class VersionCheckerQueryGenerator:
    def __init__(self, tables_map, date):
        self.date = date
        self.tables = tables_map
        self.date_column = 'market_date'
        self.keys = ['ticker', 'date']
        self.diff_type_list = ['add', 'sub', 'change']

    def generate_on_clause(self):
        on_clause = []
        for i, key in enumerate(self.keys):
            on_clause.append(f""" a.{key} = b.{key}""")
        return ' and '.join(on_clause)

    def generate_type_condition(self, columns_list):
        types_conditions = []
        for diff_type in self.diff_type_list:
            condition = []
            if diff_type == 'sub':
                condition = f""" a.{self.keys[0]} is null"""
            elif diff_type == 'add':
                condition = f""" b.{self.keys[0]} is null"""
            elif diff_type == 'change':
                for column in columns_list:
                    condition.append(f"""a.{column}<>b.{column} """)
                or_case = f"({' or '.join(condition)})"
                condition = f"a.{self.keys[0]} is not null and b.{self.keys[0]} is not null and {or_case}"
            types_conditions.append([diff_type, condition])
        return types_conditions

    def dataset_generate(self):
        table_path = '`versions_test.mid_versions`'
        x = []
        on_clause = self.generate_on_clause()
        columns_list = get_column_names_by_table(self.tables['new_table'])
        columns_list = [column for column in columns_list if 'unadjusted' in column]
        for supplier in ['alpha','yahoo','quandl']:
            select_keys = f"'{self.date}' as download_date,'{supplier}' as supplier"
            for column in columns_list:
                if supplier in column:
                    x.append(f"SELECT a.ticker,{select_keys},'{column.replace(supplier + '_', '')}' as field,b.{column} as day_ago_data,"
                        f"a.{column} as new_data,case(a.{column}>b.{column}) WHEN TRUE then "
                        f"round(abs(1- (a.{column}/(1e-10+ b.{column})))*100, 5) "
                        f"else round(abs(1-(b.{column}/(1e-10+a.{column})))*100,5) end as diff_change"
                        f" from `{self.tables['new_table']}` a full outer join "
                        f"`{self.tables['old_table']}` b on {on_clause}")
        append = self.check_append('`versions_test.avg_change_version`')
        query = f"create or replace table {table_path} as ({' union all '.join(x)})"
        query1 = f"create or replace table `versions_test.avg_change_version` as ({append}select download_date, " \
                 f"ticker,supplier, avg(diff_change) as avg_change from {table_path}" \
                 f" group by download_date, ticker,supplier)"
        query2 = self.change_size_occurrences(table_path)
        query3 = f"drop table if exists {table_path}"
        return [query,query1, query2, query3]


    def check_append(self, dest_table_path):
        if table_exists(dest_table_path):
            return f'select * from {dest_table_path} union all '
        return ''

    def change_size_occurrences(self, table_path):
        dest_table_path = '`versions_test.change_size_occurrences`'
        table2 = []
        sizes = list(np.arange(0.0, 1.0, 0.1))
        sizes_half_percent = list(np.arange(1.0, 10.5, 0.5))
        change_size_list = [[sizes[i], size] for i, size in enumerate(sizes[1:])]
        change_size_list += [[sizes_half_percent[i], size] for i, size in enumerate(sizes_half_percent[1:])] + [
            [10.0, 100000000000.0]]
        for change_size in change_size_list:
            table2.append(
                f"select supplier,download_date,'{change_size}' as change_size,field, count(*) as num_of_occurrences from "
                f"{table_path} where diff_change>{change_size[0]} and diff_change<={change_size[1]} "
                f"group by download_date,supplier, change_size, field")
        table2.append(f"select supplier,download_date,'[0.0,0.0]' as change_size,field, count(*) as num_of_occurrences from "
                f"{table_path} where diff_change=0 group by download_date,supplier, change_size, field")
        append = self.check_append(dest_table_path)
        return f"create or replace table {dest_table_path} as ({append}{' union all '.join(table2)})"
