import os
import quandl
from datetime import date
import urllib.request
import pandas as pd
from alpha_vantage.timeseries import TimeSeries
from future.backports.urllib.request import urlopen
from google.cloud import bigquery
import intrinio_sdk as intrinio
import yfinance as yf
import pandas_gbq, time
import json
import schema
from create_merged_data import set_table_expiration
from datetime import timedelta

client = bigquery.Client()
day = date.today()

def upload_data(data, source, table_prefix):
    if data == []:
        return
    table_name = f"{table_prefix}{source}"
    data = pd.concat(data)
    print('upload_data DEBUG')
    print('_'.join(list(data.keys())))
    print(data)

    schema.convert_to_schema(data, source).to_gbq(table_name,
                      project_id='finityx-app', if_exists='append')

    set_table_expiration(table_name)


def upload_data1(data, source, table_prefix):
    if data == []:
        return
    table_path = f"finityx-app.{table_prefix}{source}"
    sche = schema.get_schema_source(source)
    table = bigquery.Table(table_path, schema=sche)
    try:
        client.create_table(table)
    except Exception as e:
        print(e)
    table_id = client.get_table(table_path)
    client.insert_rows(table_id, list(pd.concat(data).values))


def upload_quandl_data(tickers_list, start_date, table_prefix):
    quandl.ApiConfig.api_key = "b5CZq3aZ2P31kHh95b87"
    quandl_data = []
    dates = pd.date_range(start_date, day, freq='d')
    for i, ticker in enumerate(tickers_list):
        if ticker.find('.') != -1:
            continue

        try:
            data: pd.DataFrame = quandl.get_table("QUOTEMEDIA/PRICES", date=','.join(dates.strftime("%Y-%m-%d")),
                                                  ticker=ticker)
        except Exception as e:
            print(e)
            continue

        print(f'percent {(i+1)/len(tickers_list)} quandl : {ticker}')
        data = data.rename(columns={'date': 'market_date'})
        data = data.reset_index()
        try:
            data = data.drop(columns = 'None')
        except:
            pass

        quandl_data.append(data)

        if len(quandl_data)>50:
            upload_data(quandl_data, 'quandl', table_prefix)
            quandl_data = []

        time.sleep(1)

    upload_data(quandl_data, 'quandl', table_prefix)


def upload_xignite_data(tickers, start_date, temp_dir, table_prefix):
    _token = 'A0CFA1BA432A4723A30D3ECFB252EE3F'
    os.makedirs(temp_dir, exist_ok=True)
    data = []
    for i, symbol in enumerate(tickers):
        print(i)
        sym = symbol  # + xignite_country_symbol[country]
        format = '.csv'
        EndDate = day.strftime('%m/%d/%Y')
        url = f'https://globalhistorical.xignite.com/v3/xGlobalHistorical{format}/GetGlobalHistoricalQuotesRange?' \
              f'IdentifierType=Symbol&Identifier={sym}&IdentifierAsOfDate=&AdjustmentMethod=All&' \
              f'StartDate={start_date}&EndDate={EndDate}&_token={_token}'
        filename = temp_dir + str(sym) + '.csv'
        try:
            urllib.request.urlretrieve(url, filename)
        except Exception as e:
            print(e)
            continue
        xignite_data = pd.read_csv(filename)
        xignite_data.columns = xignite_data.columns.str.replace(' ', '')
        data.append(xignite_data)
        os.remove(filename)
        time.sleep(1)
    upload_data(data, 'xignite', table_prefix)


def upload_marketcap(tickers_list):
    def get_jsonparsed_data(url):
        response = urlopen(url)
        data = response.read().decode("utf-8")
        return json.loads(data)

    api_key = '4ee53069f6145f3a5168e6814103612b'
    all_mkt_cap_list = []
    for ticker in tickers_list:
        try:
            url = ("https://financialmodelingprep.com/api/v3/historical-market-capitalization/" + ticker + "?limit=7500&apikey=" + api_key)
            data = get_jsonparsed_data(url)
            data_df = pd.json_normalize(data)
        except Exception as e:
            print(ticker, e)
            continue
        else:
            all_mkt_cap_list.append(data_df)
    upload_data(all_mkt_cap_list, f'{day}_market_cap', 'market_cap.')


def upload_alpha_data(tickers, table_prefix):
    ts = TimeSeries(key='QUK58TRI9CC497SJ', output_format='pandas')
    data, adjusted_data = [], []
    for i, ticker in enumerate(tickers):
        ticker1 = ticker.replace('.', '-')
        print(i,ticker)
        try:
            alpha_data, meta_data = ts.get_daily(symbol=ticker1, outputsize="full")
        except Exception as e:
            print(e)
            continue
        try:
            adjusted_alpha_data, adjusted_meta_data = ts.get_daily_adjusted(symbol=ticker1, outputsize="full")
        except Exception as e:
            print(e)
            continue

        alpha_data.columns = [i[1] for i in alpha_data.columns.str.split('. ').T]
        adjusted_alpha_data.columns = [i[1] for i in adjusted_alpha_data.columns.str.split('. ').T]
        alpha_data['market_date'] = list(alpha_data.index)
        adjusted_alpha_data['market_date'] = list(adjusted_alpha_data.index)
        alpha_data['ticker'] = ticker
        adjusted_alpha_data['ticker'] = ticker

        alpha_data = alpha_data.reset_index()
        adjusted_alpha_data = adjusted_alpha_data.reset_index()
        try:
            del alpha_data['date']
        except:
            pass

        try:
            del adjusted_alpha_data['date']
        except:
            pass

        data.append(alpha_data)
        adjusted_data.append(adjusted_alpha_data)
        time.sleep(1)

    upload_data(data, 'alpha', table_prefix)
    upload_data(adjusted_data, 'alpha_adjusted', table_prefix)


def upload_intrinio_data(tickers, start_date, table_prefix):
    intrinio.ApiClient().configuration.api_key['api_key'] = 'OmQ3ODAxOTNiOTQwYzUyZjJhYzFkNGRmZGUzM2QwMjRh'
    data = []
    for i, ticker in enumerate(tickers):
        print(i,ticker)
        frequency = 'daily'
        page_size = 9999
        try:
            response = intrinio.SecurityApi().get_security_stock_prices(identifier=ticker,
                                                                        start_date=start_date, end_date=day,
                                                                        frequency=frequency, page_size=page_size)
        except Exception as e:
            print(f'skipped {ticker}')
            print(e)
            continue
        intrinio_data = pd.DataFrame(response.stock_prices_dict)
        intrinio_data['ticker'] = ticker
        data.append(intrinio_data)
        time.sleep(1)

    upload_data(data, 'intrinio', table_prefix)


def upload_yahoo_data(tickers, start_date, table_prefix):
    yf.pdr_override()
    data = []
    yesterday = day - timedelta(days=1)
    for i, ticker in enumerate(tickers):
        ticker = ticker.replace(".", "-")
        try:
            yahoo_data = yf.Ticker(ticker).history(start=start_date, end=yesterday, auto_adjust=False)
        except Exception as e:
            print(f'skipped {ticker}')
            print(e)
            continue

        yahoo_data['market_date'] = list(yahoo_data._stat_axis)
        yahoo_data['ticker'] = ticker
        yahoo_data = yahoo_data.reset_index()
        try:
            yahoo_data = yahoo_data.drop(columns = 'Date')
        except:
            pass

        yahoo_data.columns = yahoo_data.columns.str.replace(' ', '')
        time.sleep(1)
        data.append(yahoo_data)
        print(f'percent {(i+1)/len(tickers)} yahoo : {ticker}')

        if len(data) > 50:
            print('upload')
            upload_data(data, 'yahoo', table_prefix)
            data = []

    upload_data(data, 'yahoo', table_prefix)


def upload_ticker(tickers_list, start_date, source, table_prefix):
    TEMP_DIR = r'/tmp/'
    t = time.time()
    if source == 'marketcap':
        upload_marketcap(tickers_list)
    if source == 'quandl':
        upload_quandl_data(tickers_list, start_date, table_prefix)
    if source == 'yahoo':
        upload_yahoo_data(tickers_list, start_date, table_prefix)
    if source == 'intrinio':
        upload_intrinio_data(tickers_list, start_date, table_prefix)
    if source == 'alpha':
        upload_alpha_data(tickers_list, table_prefix)
    if source == 'xignite':
        upload_xignite_data(tickers_list, start_date, TEMP_DIR, table_prefix)
    dt = time.time() - t

    print(f'upload_{source}_data {dt}')
