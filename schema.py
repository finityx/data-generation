from google.cloud import bigquery

schemas = {}

schemas['alpha'] = {'open': float, 'high': float, 'low': float, 'close': float, 'volume': float}

schemas['alpha_adjusted'] = {'open': float, 'high': float, 'low': float, 'close': float, 'volume': float,
                             'adjuste': float, 'dividen': float, 'spli': float}

schemas['intrinio'] = {'date': str, 'intraperiod': bool, 'frequency': str, 'open': float, 'high': float, 'low': float,
                       'close': float, 'volume': float,
                       'adj_open': float, 'adj_high': float, 'adj_low': float, 'adj_close': float, 'adj_volume': float,
                       'ticker': str}

schemas['quandl'] = {'ticker': str, 'open': float, 'high': float, 'low': float, 'close': float, 'volume': float, 'dividend': float,
                     'split': float, 'adj_open': float,
                     'adj_high': float, 'adj_low': float, 'adj_close': float, 'adj_volume': float}

schemas['yahoo'] = {'Open': float, 'High': float, 'Low': float, 'Close': float, 'AdjClose': float, 'Volume': float,
                    'Dividends': float, 'StockSplits': float, 'ticker': str}

schemas['xignite'] = {'Outcome': str, 'Message': str, 'Identity': str, 'Delay': float, 'Identifier': str,
                      'IdentifierType': str, 'SecurityCIK': str, 'SecurityCUSIP': str,
                      'SecuritySymbol': str, 'SecurityISIN': str, 'SecurityValoren': str, 'SecuritySEDOL': str,
                      'SecurityFIGI': str, 'SecurityName': str, 'SecurityInstrumentClass': str,
                      'SecurityMarket': str, 'SecurityMarketIdentificationCode': str, 'SecuritySector': str,
                      'SecurityIndustry': str, 'SecurityCompositeFIGI': str, 'StartDate': str,
                      'EndDate': str, 'HistoricalQuotesDate': str, 'HistoricalQuotesClose': float,
                      'HistoricalQuotesOpen': float, 'HistoricalQuotesHigh': float, 'HistoricalQuotesLow': float,
                      'HistoricalQuotesVWAP': float, 'HistoricalQuotesTWAP': float, 'HistoricalQuotesVolume': float,
                      'HistoricalQuotesPreviousClose': float, 'HistoricalQuotesPreviousCloseDate': str,
                      'HistoricalQuotesChange': float, 'HistoricalQuotesPercentChange': float,
                      'HistoricalQuotesAdjustmentMethodUsed': str, 'HistoricalQuotesCumulativeAdjustmentFactor': float,
                      'HistoricalQuotesCurrency': str, 'HistoricalQuotesPriceNotation': str,
                      'HistoricalQuotesPriceAmount': float}
types = {str: 'STRING', float: 'FLOAT64'}


def convert_to_schema(df, source):
    for field, field_type in schemas[source].items():
        if field in df:
            df[field] = df[field].astype(field_type)
    return df


def get_schema_source(source):
    schema = []
    for key in schemas[source].keys():
        schema.append(bigquery.SchemaField(key, types[schemas[source][key]]))
    return schema
