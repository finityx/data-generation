import os

import pandas_gbq
import requests
from google.cloud import bigquery
import time
import json
from google.cloud import pubsub_v1
from datetime import date, timedelta, datetime,timezone
import pandas_market_calendars as mcal
import yfinance as yf
from checked_table import run
from create_merged_data import create_merged_table
import pandas as pd
from pandas_datareader import data as pdr
import pytz
import upload_ticker
from yahoofinancials import YahooFinancials
from multiprocessing.pool import ThreadPool as Pool

client = bigquery.Client()
sources_list = ['yahoo','quandl', 'alpha']
is_using_debug_date = False
try:
    if os.environ['DEBUG_DATE'] == '':
        day = date.today()
    else:
        day = date.fromisoformat(os.environ['DEBUG_DATE'])
        is_using_debug_date = True
except:
    day = date.today()

def download_late_close():

    MINUTES_BEFORE_CLOSE = 15

    nyse = mcal.get_calendar('NYSE')

    valid_dates = nyse.valid_days(start_date=day, end_date=(day + timedelta(days=500)))[
                  0:(1 + 1)]
    close_time = pd.to_datetime(nyse.schedule(valid_dates[0], valid_dates[0]).market_close.to_list())[0]
    start_time = close_time - timedelta(minutes=MINUTES_BEFORE_CLOSE)

    t = datetime.now(tz=pytz.UTC)
    tt = start_time - t

    is_live = day == date.today()
    if is_live:
        if tt.days >= 0:
            print(f"now is {t}. start time for extraction is {start_time}. waiting for {tt.seconds} seconds")
            time.sleep(tt.seconds)

    curr_datetime_str = day.strftime('%Y-%m-%d')

    live_tickers =  ['QCOM', 'GOOG', 'CSCO', 'V', 'XLU', 'DHR', 'AAL', 'ENB', 'IYR', 'MDLZ', 'JNJ', 'HD', 'LMT', 'KO', 'LQD', 'TLT', 'SMH', 'XOP', 'IBM', 'DBC', 'NVDA', 'GLD', 'ADI', 'CVS', 'MSFT', 'IEI', 'GDX', 'MDT', 'XLY', 'XLB', 'MMM', 'DIS', 'XOM', 'PEP', 'VZ', 'DBB', 'XLP', 'SBUX', 'VNQ', 'LUV', 'GS', 'CVX', 'INTC', 'GOOGL', 'CSV', 'MA', 'USO', 'BAC', 'CAT', 'SPY', 'AXP', 'SOXX', 'VIXY', 'SHW', 'CI', 'UAL', 'PG', 'DAL', 'HON', 'NKE', 'AMD', 'XLF', 'KRE', 'PM', 'MU', 'SO', 'AAPL', 'QQQ', 'NOC', 'DE', 'PFE', 'XLE', 'ITA', 'XLV', 'TMO', 'XLK', 'MO', 'JPM', 'UNH', 'WBA', 'LIN', 'OIH', 'C', 'ADP', 'XLI', 'XME', 'WMT', 'PGR', 'MCD', 'LLY', 'DBA', 'BA', 'MRK', 'AMZN', 'COST', 'DIA']
    pool = Pool(100)


    def download_ticker(t_i):
        i, t = t_i
        tm = time.time()
        response = requests.get(f"https://api.polygon.io/v2/aggs/ticker/{t}/range/1/minute/{day}/{day}?adjusted=true&sort=asc&limit=1000&apiKey=3mRdnUnly2eO45uhoyqVEyp_MpSYsbo_")
        x = response.json()
        print(i, tm - time.time())

        if 'results' in x.keys():
            tx = pd.DataFrame({k: [r[k] for r in x['results']] for k in x['results'][0]})
            tx['ticker'] = x['ticker']
            return tx
        return None



    def upload_table():

        results = pool.map(download_ticker, list(enumerate(live_tickers)))
        polygon_df = pd.concat([r for r in results if r is not None])
        polygon_df['t'] = pd.to_datetime(polygon_df['t'], unit='ms')
        polygon_df = polygon_df[polygon_df['o'] != 0]

        df = pd.DataFrame([])
        df['real_date']   =  polygon_df['t']
        df['ticker'] = polygon_df['ticker']
        df['close'] = polygon_df['c']
        df['open'] = polygon_df['o']
        df['high'] = polygon_df['h']
        df['low'] = polygon_df['l']
        df['upload_time'] = datetime.utcnow()
        df['date'] = df.upload_time.dt.floor(freq='min')
        df = pd.merge(df.groupby('ticker').real_date.max().reset_index(), df, how='left', on=['ticker', 'real_date'])
        dates = sorted(df['date'].dt.date.unique())

        assert (len(dates) == 1) and (pd.to_datetime(max(dates)).date()==datetime.utcnow().date()),"DATA BUG dates in late "

        df.to_gbq(f'latest_close_prices.{curr_datetime_str}', if_exists='append')

    while not is_live or datetime.now(tz=pytz.UTC)<=close_time:
        upload_table()
        time.sleep(30)
        if not is_live:
            break


def beta_calc(table_name):
    bigqueryclient = bigquery.Client()
    local = pytz.timezone("UTC")


    def get_ticker(tick):
        ticker = bigqueryclient.query(f"""select date as date,
                                                yahoo_unadjusted_close*yahoo_adjustment_factor as Close,
    --                                             unadjusted_open*adjustment_factor as Open,
    --                                             unadjusted_high*adjustment_factor as High,
    --                                             unadjusted_low*adjustment_factor as Low,
    --                                             unadjusted_volume*unadjusted_close as Volume,
                                                from `{table_name}` where ticker='{tick}'""").result().to_dataframe()
        ticker.date = ticker.date.dt.date
        ticker = ticker.set_index('date')
        ticker = ticker.sort_values(by='date')
        ticker['revenue'] = (ticker.Close.shift(-1) / ticker.Close) - 1
        return ticker

    SPY = get_ticker('SPY')


    tickers = bigqueryclient.query(f"""select distinct ticker from `{table_name}`""").result().to_dataframe().ticker.tolist()
    ticker_groups = []
    for i in range(0,len(tickers),50):
        ticker_groups.append(tickers[i:i+50])

    start_time = time.time()
    first = True

    for ticker_group in ticker_groups:
        if len(ticker_group)==1:
            all_ticker = bigqueryclient.query(f"""select date ,ticker ,yahoo_unadjusted_close*yahoo_adjustment_factor as Close from `{table_name}` where ticker = '{ticker_group[0]}'""").result().to_dataframe()
        else:
            all_ticker = bigqueryclient.query(f"""select date ,ticker ,yahoo_unadjusted_close*yahoo_adjustment_factor as Close from `{table_name}` where ticker in {tuple(ticker_group)}""").result().to_dataframe()
        all_ticker.date = all_ticker.date.dt.date
        g_tickers = all_ticker.groupby('ticker')
        all_tickers_new = []

        for name, ticker in g_tickers:
            print(name,time.time()-start_time)
            ticker = ticker.set_index('date')
            ticker = ticker.sort_values(by='date')
            ticker['revenue'] = (ticker.Close.shift(-1) / ticker.Close) - 1
            ticker = pd.merge(ticker, SPY, how="left", on='date', suffixes=("", "_SPY"))
            ticker['beta'] = (ticker.revenue.ewm(halflife=21).cov(other=ticker.revenue_SPY) / ticker.revenue_SPY.ewm(halflife=21).var())
            ticker['std'] = ticker.revenue.ewm(halflife=21).std() / ticker.revenue_SPY.ewm(halflife=21).std()
            all_tickers_new.append(ticker)

        all_tickers_new = pd.concat(all_tickers_new)
        all_tickers_new = all_tickers_new.reset_index()
        all_tickers_new['date'] = all_tickers_new['date'].astype(str)
        if first:
            all_tickers_new.to_gbq(f'BETA.curr_beta_{day}', if_exists='replace')
            all_tickers_new.to_gbq(f'BETA.curr_beta', if_exists='replace')

        else:
            all_tickers_new.to_gbq(f'BETA.curr_beta_{day}', if_exists='append')
            all_tickers_new.to_gbq(f'BETA.curr_beta', if_exists='append')

        first = False


def get_tickers_list():
    query_result = client.query("select distinct ticker from starmine.all_companies_ultimate_mapping ORDER by ticker").result()
    return [res[0] for res in query_result]


def publish_msg(publisher, topic_path, message_json):
    message_bytes = message_json.encode('utf-8')
    publish_future = publisher.publish(topic_path, data=message_bytes)
    publish_future.result()
    time.sleep(1)


def get_column_names_by_table(tbl):
    query = """select * from `%s` LIMIT 0""" % tbl
    query_job = client.query(query)
    return [column.name for column in query_job.result().schema]


def get_missing_tickers(source, day2):
    ticker = 'ticker'
    if source == 'xignite':
        ticker = 'Identifier'
    query1 = f"select distinct b.ticker from stocks.{source}_tickers b " \
             f"left join `stocks.{day2}{source}` a on a.{ticker}=b.ticker where a.{ticker} is null"
    tickers_res = client.query(query1).result()
    return [tick[0] for tick in tickers_res]


def function_trigger(function_name, input_function_dict, tickers, publisher, topic_path, ntickers):
    for k in range(0, len(tickers), ntickers):
        print(f'function_trigger : {100*(1+k)/len(tickers)}')
        # upload_ticker.upload_ticker(tickers_list=tickers[k:(k + ntickers)], start_date=input_function_dict['start_date'], source=input_function_dict['source'],
        #                              table_prefix=input_function_dict['table_prefix'])

        input_function_dict['tickers_list'] = tickers[k:(k + ntickers)]
        message_json = json.dumps(
            {'function_name': function_name,
             'input_function': input_function_dict})
        publish_msg(publisher, topic_path, message_json)
        time.sleep(8)


def complete_missing_tickers(source, table_prefix, publisher, topic_path, day2):
    retry = 5
    missing_tickers = get_missing_tickers(source, day2)
    last_missing_count = 0
    while missing_tickers != [] and retry != 0 and (len(missing_tickers)!=last_missing_count):
        retry -= 1
        function_trigger('upload_ticker', {'start_date': '2000-01-01', 'source': source, 'table_prefix': table_prefix},
                         missing_tickers, publisher, topic_path, ntickers=25)
        time.sleep(120)
        last_missing_count = len(missing_tickers)
        missing_tickers = get_missing_tickers(source, day2)



def get_divedends_splits_changes(df1, df2, day2):
    tickers_divedends_splits, tickers_list1, tickers_list2 = [], [], []
    last_date_list = df2.loc[df2['market_date'] == str(day2)][["ticker", 'adjustment_factor']].values.tolist()
    current_date_list = df1.loc[df1['market_date'] == str(day2)][["ticker", 'adjustment_factor']].values.tolist()
    if current_date_list:
        tickers_list1 = list(zip(*current_date_list))[0]
    if last_date_list:
        tickers_list2 = list(zip(*last_date_list))[0]
    diff = list(set(tickers_list1) - set(tickers_list2)) + list(set(tickers_list2) - set(tickers_list1))
    last_date_list, current_date_list = [d for d in last_date_list if d[0] not in diff], [d for d in current_date_list
                                                                                          if d[0] not in diff]
    for i, d in enumerate(last_date_list):
        if abs(1 - d[1] / current_date_list[i][1]) > 0.001:
            tickers_divedends_splits.append(d[0])
    return tickers_divedends_splits


def missing_tickers(day2, suffix):
    return client.query(f"""SELECT
      a.ticker
    FROM (
      SELECT
        a.ticker
      FROM
        (select distinct ticker from `finityx-app.checked_data.{day2}_open_merged_stocks{suffix}` where  date = '{day2}') b
      RIGHT JOIN
        `finityx-app.starmine.all_companies_ultimate_mapping` a
      ON
        a.ticker = b.ticker
      WHERE
        b.ticker IS NULL) a
    LEFT JOIN (
      SELECT
        *
      FROM
        `finityx-app.mappings.problematic_tickers`
      UNION ALL
      SELECT
        *
      FROM
        `finityx-app.mappings.unactive_tickers`) b on a.ticker = b.ticker where b.ticker is null""").result().to_dataframe()


def upload_market_cap(tickers, yesterday):
    upload_time = datetime.now().isoformat()
    table = f"finityx-app.stocks.qad_market_cap_complete"
    l = []
    for i, ticker in enumerate(tickers):
        ticker2 = ticker = ticker.replace(".", "-")
        try:
            market_cap_data = pd.DataFrame(pdr.get_quote_yahoo(ticker2)['marketCap'])
            t = str(yesterday), market_cap_data.values[0][0] / 1000000, ticker, upload_time
            l.append(t)
        except Exception as e:
            print(e)
        if i % 10 == 5 and l:
            print(f"market_cap {i}")
            values = str(l)[1:-1]
            client.query(f"""INSERT into {table} (date,ConsolMktVal,ticker,upload_time) values {values}""").result()
            l = []
    client.query(f"""INSERT into {table} (date,ConsolMktVal,ticker,upload_time) values {values}""").result()


def trigger_daily():
    tickers = get_tickers_list()
    publisher = pubsub_v1.PublisherClient()
    project_id = 'finityx-app'
    topic_path = publisher.topic_path(project_id, 'data_generation')

    table_prefix = f'stocks.{day}'
    checked_table = f"checked_data.{day}_checked_stocks"
    for source in sources_list:
        client.query(f"drop table if exists `{table_prefix}{source}`").result()
        function_trigger('upload_ticker', {'start_date': '2000-01-01', 'source': source, 'table_prefix': table_prefix},
                         tickers, publisher, topic_path, ntickers=25)
        print(f"finish tickers upload for {source}")

    for source in sources_list:
        complete_missing_tickers(source, table_prefix, publisher, topic_path, day)
        print(f"missing upload for {source}")

    print("waiting 10 minutes")
    time.sleep(801)
    print("finish waiting")
    client.query(f"drop table if exists `checked_data.{day}_merged_stocks`").result()
    create_merged_table(day)
    print(f"finish create merged table")
    time.sleep(800)
    client.query(f"drop table if exists `{checked_table}`").result()
    run(day)
    # function_trigger("run", {'day': str(day)}, list(range(1, 4799)), publisher,topic_path,ntickers=70)
    print(f"finish create checked table")
    nyse = mcal.get_calendar('NYSE')
    last_market_date = nyse.valid_days(start_date=day - timedelta(days=4), end_date=day)[-2]
    try:
        print(f"set problems missing points for checked_data")
        try:
            upload_market_cap(tickers, last_market_date.date())
            # function_trigger('fill_missing_rows', {'checked_table': checked_table,'day': str(day)},tickers, publisher, topic_path, ntickers=70)
            add_columns(checked_table)
            print("finish add columns")
            table_id = "checked_data.checked_stocks_indices"
            df = client.query(
                f"""select * from `stocks.{day}_yahoo_uniform` where ticker in ('^VIX', '^GLD', '^SPX', '^DJI', '^IXIC', '^RUT')""").result().to_dataframe()
            pandas_gbq.to_gbq(df, table_id, project_id='finityx-app', if_exists='replace')
            data = requests.get(
                "https://stocknewsapi.com/api/v1/category?section=alltickers&items=50&token=5ncb8wzoyspkt5nqew5dpkc1guxt8o8rexh0s5dm").json()
            if 'data' in data:
                pandas_gbq.to_gbq(pd.DataFrame(data['data']), 'news.stock_news_api', project_id='finityx-app',
                                  if_exists="append")
            monitor = []
            montior_table = 'metrics.success_data'
            x = datetime.now()
            for source in sources_list:
                monitor.append(
                    f"select count(distinct ticker) as success, '{x}' as date, '{source}' as supplier,'stocks' as dataset, 'open' as mode from `stocks.{day}{source}`")
            client.query(f"insert into `{montior_table}` ({' union all '.join(monitor)})").result()
            monitor = []
            montior_table = 'metrics.failed_data'
            for source in sources_list:
                monitor.append(
                    f"select count(distinct a.ticker) as failed, '{x}' as date, '{source}' as supplier, 'stocks' as dataset, 'open' as mode from `stocks.{day}{source}`"
                    f" a right join stocks.{source}_tickers b on a.ticker=b.ticker where b.ticker is null")
            client.query(f"insert into `{montior_table}` ({' union all '.join(monitor)})").result()
            client.query(
                f"create or replace table `metrics.data_loader_monitor` as (SELECT b.failed, a.* except(date),cast(a.date as datetime) as date FROM `finityx-app.metrics.success_data` "
                f"as a join `finityx-app.metrics.failed_data` b on a.dataset = b.dataset and a.date = b.date and a.supplier = b.supplier and a.mode =b.mode)").result()
        except Exception as e:
            print(e)

    except:
        print('ERROR: uncrusial data stuff crushed')

    merged_table = f"checked_data.{day}_merged_stocks"
    for tbl in [merged_table, checked_table]:
        client.query(f"drop table if exists `{tbl}_etf`")
        client.query(
            f"""create or replace table `{tbl}_etf` PARTITION BY
    RANGE_BUCKET(ticker_index, GENERATE_ARRAY(0, 5000, 2)) as(select * from `{tbl}` where ticker in (select symbol from etf.etf_mappings));""").result()
    monitor_check(merged_table)

    if is_using_debug_date:
        client.query(f"""DELETE FROM `{merged_table}` WHERE date>'{day}'""").result()
        client.query(f"""DELETE FROM `{checked_table}` WHERE date>'{day}'""").result()

    print("finish ")


def add_columns(checked_table):
    print("upload_market_cap")
    sql = f"""create or replace table `{checked_table}` PARTITION BY
    RANGE_BUCKET(ticker_index, GENERATE_ARRAY(0, 5000, 2))
    as (select distinct a.*, ConsolMktVal as market_cap,
    lvl1_code as ind_code_lvl1,lvl2_code as ind_code_lvl2 from `{checked_table}` a
    left join stocks.qad_market_cap_complete b on a.ticker = b.ticker and a.date = CAST (b.date as TIMESTAMP)
    left join mappings.industry c on a.ticker = c.ticker)"""
    client.query(sql).result()
    print("finish market_cap")


# def create_table_calendar_trading(day):
#     values = []
#     tickers_list = get_tickers_list()
#     for i,ticker in enumerate(tickers_list):
#         print(i)
#         for market_date in all_market_dates:
#             values.append((ticker,str(market_date)[:10]))
#     df = pd.DataFrame(values,columns=['ticker', 'market_date'])
#     pandas_gbq.to_gbq(df, "stocks.calendar_trading",project_id='finityx-app', if_exists='replace')


def fill_missing_rows(checked_table, day2, tickers_list):
    t = 'stocks.all_trading_days'
    nyse = mcal.get_calendar('NYSE')
    all_market_dates = pd.DataFrame([i.date() for i in nyse.valid_days(start_date='2000-01-01', end_date=day2)],
                                    columns=['date'])
    pandas_gbq.to_gbq(all_market_dates, 'stocks.all_trading_days', project_id='finityx-app', if_exists='replace',
                      table_schema=[{'name': 'date', 'type': 'DATE'}])
    # client.query(f"create or replace table {t} as (select cast(date as date) as date from {t})")
    d = []
    for i, ticker in enumerate(tickers_list):
        df = client.query(
            f"""select distinct * from `{checked_table}` where ticker = '{ticker}' order by date""").result().to_dataframe()
        df.set_index('date', inplace=True)
        mask = df.index.duplicated()
        df1 = df.loc[~mask].resample('D').ffill().reset_index()
        df2 = all_market_dates.merge(df1, left_on='date', right_on='date', how='inner')
        df = df.reset_index()
        diff = df.merge(df2, indicator=True, how='right').loc[lambda x: x['_merge'] != 'both']
        diff = diff.rename(columns={'_merge': 'duplicate_flag'})
        diff['duplicate_flag'] = 1
        print(len(df2), len(df))
        d.append(diff)
    pandas_gbq.to_gbq(pd.concat(d), checked_table, project_id='finityx-app', if_exists='append')


def table_exists(table_path):
    try:
        client.get_table(table_path)
        return True
    except Exception as e:
        return False


def insert_by_csv(tickers_list, suffix='_csv'):
    client = bigquery.Client()

    table_id = f"finityx-app.checked_data.{day}_open_checked_stocks{suffix}"
    table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks{suffix}"
    t = []
    for i, ticker in enumerate(tickers_list):
        ticker2 = ticker
        ticker = ticker.replace(".", "-")
        try:
            yahoo_data = yf.Ticker(ticker).history(start=day, end=day + timedelta(days=1), auto_adjust=False)
        except Exception as e:
            print(e)
            continue
        if yahoo_data.size == 8:
            yahoo_data1 = list(yahoo_data[list(yahoo_data.columns[:-4])].values[-1])
            data = yahoo_data1 + [yahoo_data[['Volume']].values[0][0], str(datetime.combine(day, datetime.min.time())),
                                  ticker2]
            t.append(tuple(data))
            if len(t) % 10 == 5 and t:
                print(f"open {i}")
                values = str(t)[1:-1]
                client.query(
                    f"""INSERT into `{table_id}`(unadjusted_open,unadjusted_high,unadjusted_low,unadjusted_close,unadjusted_volume,date,ticker) values {values}""").result()
                client.query(
                    f"""INSERT into `{table_id_merged}`(yahoo_unadjusted_open,yahoo_unadjusted_high,yahoo_unadjusted_low,yahoo_unadjusted_close,yahoo_unadjusted_volume,date,ticker) values {values}""").result()
                t = []
        else:
            print("here")
    if t:
        values = str(t)[1:-1]
        client.query(
            f"""INSERT into `{table_id}`(unadjusted_open,unadjusted_high,unadjusted_low,unadjusted_close,unadjusted_volume,date,ticker) values {values}""").result()
        client.query(
            f"""INSERT into `{table_id_merged}`(yahoo_unadjusted_open,yahoo_unadjusted_high,yahoo_unadjusted_low,yahoo_unadjusted_close,yahoo_unadjusted_volume,date,ticker) values {values}""").result()

def insert_fast(tickers_list, suffix='_fast'):
    start_time = time.time()
    client = bigquery.Client()

    table_id = f"finityx-app.checked_data.{day}_open_checked_stocks{suffix}"
    table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks{suffix}"

    yahoo_data = YahooFinancials(tickers_list).get_historical_price_data(start_date=day.isoformat(), end_date=(day + timedelta(days=1)).isoformat(), time_interval = 'daily')

    values = [(v['prices'][-1]['open'],v['prices'][-1]['high'],v['prices'][-1]['low'],v['prices'][-1]['close'],v['prices'][-1]['volume'],v['prices'][-1]['formatted_date'],k) for k,v in yahoo_data.items() if 'prices' in v and v['prices'] != []]
    values = str(values).replace('[','').replace(']','')
    client.query(
        f"""INSERT into `{table_id}`(unadjusted_open,unadjusted_high,unadjusted_low,unadjusted_close,unadjusted_volume,date,ticker) values {values}""").result()
    client.query(
        f"""INSERT into `{table_id_merged}`(yahoo_unadjusted_open,yahoo_unadjusted_high,yahoo_unadjusted_low,yahoo_unadjusted_close,yahoo_unadjusted_volume,date,ticker) values {values}""").result()

    print(len(values),time.time()-start_time)


def insert_open_yahoo(tickers_list):
    client = bigquery.Client()

    table_id = f"finityx-app.checked_data.{day}_open_checked_stocks"
    table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks"
    t = []
    for i, ticker in enumerate(tickers_list):
        ticker2 = ticker
        ticker = ticker.replace(".", "-")
        try:
            data = pdr.get_data_yahoo(ticker)
        except Exception as e:
            print(e)
            continue
        arr1 = data.values[-1][:-1]
        x = arr1[0], arr1[1], arr1[2], arr1[3], arr1[4], data.index[-1], ticker2
        t.append(x)
        if i % 10 == 5 and t:
            print(f"open {i}")
            values = str(t)[1:-1]
            client.query(
                f"""INSERT into `{table_id}`(unadjusted_high,unadjusted_low,unadjusted_open,unadjusted_close,unadjusted_volume,date,ticker) values {values}""").result()
            client.query(
                f"""INSERT into `{table_id_merged}`(yahoo_unadjusted_high,yahoo_unadjusted_low,yahoo_unadjusted_open,yahoo_unadjusted_close,yahoo_unadjusted_volume,date,ticker) values {values}""").result()
            t = []
    values = str(t)[1:-1]
    client.query(
        f"""INSERT into `{table_id}` (unadjusted_high,unadjusted_low,unadjusted_open,unadjusted_close,unadjusted_volume,date,ticker) values {values}""").result()
    client.query(
        f"""INSERT into `{table_id_merged}` (yahoo_unadjusted_high,yahoo_unadjusted_low,yahoo_unadjusted_open,yahoo_unadjusted_close,yahoo_unadjusted_volume,date,ticker) values {values}""").result()


def pre_open(tickers, function_name, publisher, topic_path, suffix):

    table_id = f"finityx-app.checked_data.{day}_open_checked_stocks{suffix}"
    source_merged_table_id = f"finityx-app.checked_data.{day}_merged_stocks"
    source_table_id = f"finityx-app.checked_data.{day}_checked_stocks"
    table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks{suffix}"
    client.query(f"drop table if exists `{table_id}`").result()
    client.query(f"drop table if exists `{table_id_merged}`").result()
    client.query(
        f"create table `{table_id}` PARTITION BY RANGE_BUCKET(ticker_index, GENERATE_ARRAY(0, 5000, 2)) as (select * from `{source_table_id}`)").result()
    client.query(
        f"create table `{table_id_merged}` PARTITION BY RANGE_BUCKET(ticker_index, GENERATE_ARRAY(0, 5000, 2)) as (select * from `{source_merged_table_id}`)").result()
    columns_name_merged = get_column_names_by_table(table_id_merged)
    columns_name = get_column_names_by_table(table_id)
    columns_name = [column for column in columns_name if column != 'ticker_index' and column != 'ticker']
    columns_name_merged = [column for column in columns_name_merged if column != 'ticker_index' and column != 'ticker']
    for i in list(range(0, 3)):
        if tickers:
            function_trigger(function_name, {}, tickers, publisher, topic_path, ntickers=25)
            time.sleep(120)
        tickers = missing_tickers(day, suffix)
        tickers = list(tickers.T.values[0])
    time.sleep(1200)
    client.query(f"""create or replace table `{table_id_merged}`  PARTITION BY
     RANGE_BUCKET(ticker_index, GENERATE_ARRAY(0, 5000, 2)) as(SELECT
        a.ticker_index, a.ticker, {','.join(columns_name_merged)}
      FROM (select ticker, row_number() OVER() as ticker_index from(
          SELECT
            distinct ticker
          FROM `{table_id_merged}`)) AS a
      JOIN
        `{table_id_merged}` AS b
      ON
        a.ticker=b.ticker group by a.ticker_index, a.ticker, {','.join(columns_name_merged)})""").result()
    client.query(f"""create or replace table `{table_id}`  PARTITION BY
     RANGE_BUCKET(ticker_index, GENERATE_ARRAY(0, 5000, 2)) as(SELECT
        a.ticker_index, a.ticker, {','.join(columns_name)}
      FROM (select ticker, row_number() OVER() as ticker_index from(
          SELECT
            distinct ticker
          FROM `{table_id}`)) AS a
      JOIN
        `{table_id}` AS b
      ON
        a.ticker=b.ticker group by a.ticker_index, a.ticker, {','.join(columns_name)})""").result()

    if function_name == 'insert_open_yahoo':
        x = datetime.now()
        select_fields = f"'{x}' as date, 'yahoo' as supplier, 'stocks' as dataset, 'close' as mode"
        monitor = f"select count(*) as success, {select_fields} from `{table_id}` where date='{day}'"
        montior_table = 'metrics.success_data'
        client.query(f"insert into `{montior_table}` ({monitor})").result()
        monitor = f"select count(a.ticker) as failed, {select_fields}" \
                  f" from `finityx-app.stocks.yahoo_tickers` a left join " \
                  f"`checked_data.{day}_open_merged_stocks` b on a.ticker = b.ticker where b.ticker is null"
        montior_table = 'metrics.failed_data'
        client.query(f"insert into `{montior_table}` ({monitor})").result()
        client.query(
            f"create or replace table `metrics.data_loader_monitor` as (SELECT b.failed, a.* except(date),cast(a.date as datetime) as date FROM `finityx-app.metrics.success_data` "
            f"as a join `finityx-app.metrics.failed_data` b on a.dataset = b.dataset and a.date = b.date and a.supplier = b.supplier and a.mode =b.mode)").result()
        for tbl in [table_id_merged, table_id]:
            client.query(f"drop table if exists `{tbl}_etf`")
            client.query(
                f"""create or replace table `{tbl}_etf` PARTITION BY
     RANGE_BUCKET(ticker_index, GENERATE_ARRAY(0, 5000, 2)) as(select * from `{tbl}` where ticker in (select symbol from etf.etf_mappings));""").result()
        monitor_check(table_id_merged)

    if is_using_debug_date:
        client.query(f"""DELETE FROM `{table_id}` WHERE date>'{day}'""").result()
        client.query(f"""DELETE FROM `{table_id_merged}` WHERE date>'{day}'""").result()


def create_ib_table(tickers,suffix):

    source_merged_table_id = f"finityx-app.checked_data.{day}_merged_stocks"
    table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks{suffix}"
    client.query(f"drop table if exists `{table_id_merged}`;create table `{table_id_merged}` as (select * from `{source_merged_table_id}`)").result()

    tickers_index = client.query(f'select distinct(ticker),ticker_index  from `{source_merged_table_id}`').to_dataframe()
    tickers_index = dict(zip(tickers_index.ticker, tickers_index.ticker_index))
    broker_data = client.query(f"select extract(date from date) as date,ticker,open from `checked_data.open_IB` where ticker in {tuple(tickers)} and open > 0 and extract(date from date) = '{day}'").to_dataframe()
    broker_data = broker_data[broker_data['ticker'].isin(list(tickers_index.keys()))]
    df = pd.DataFrame([])
    df['date'] = pd.to_datetime(broker_data['date'])
    df['ticker'] = broker_data['ticker']
    df['ticker_index'] = [tickers_index[t] for t in broker_data['ticker']]
    df['yahoo_unadjusted_open'] = broker_data['open']
    df.to_gbq(table_id_merged.lstrip('finityx-app.'),if_exists='append')

def create_polygon_table(tickers,suffix):

    source_merged_table_id = f"finityx-app.checked_data.{day}_merged_stocks"
    table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks{suffix}"
    client.query(f"drop table if exists `{table_id_merged}`;create table `{table_id_merged}` as (select * from `{source_merged_table_id}`)").result()

    tickers_index = client.query(f'select distinct(ticker),ticker_index  from `{source_merged_table_id}`').to_dataframe()
    tickers_index = dict(zip(tickers_index.ticker, tickers_index.ticker_index))
    pool = Pool(100)

    def download_ticker(t_i):
        i, t = t_i
        tm = time.time()
        response = requests.get(f"https://api.polygon.io/v2/aggs/ticker/{t}/range/1/day/{day}/{day}?adjusted=true&sort=asc&limit=120&apiKey=3mRdnUnly2eO45uhoyqVEyp_MpSYsbo_")
        x = response.json()
        print(i, tm - time.time())
        if 'results' in x.keys():
            x.update(x['results'][0])
            return pd.DataFrame(x)
        return None

    results = pool.map(download_ticker,list(enumerate(tickers)))
    polygon_df=pd.concat([r for r in results if r is not None])
    polygon_df  = polygon_df[polygon_df['o']!=0]
    polygon_df = polygon_df[polygon_df['ticker'].isin(list(tickers_index.keys()))]

    df = pd.DataFrame([])
    df['ticker'] = polygon_df['ticker']
    df['ticker_index'] = [tickers_index[t] for t in polygon_df['ticker']]
    df['yahoo_unadjusted_open'] = polygon_df['o']
    df['date'] = pd.to_datetime(day)

    df.to_gbq(table_id_merged.lstrip('finityx-app.'),if_exists='append')

def choose_data_table_for_open(suffix):

    table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks{suffix}"
    try:
        ch_table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks_polygon"
        client.query(f"drop table if exists `{table_id_merged}`;create table `{table_id_merged}` as (select * from `{ch_table_id_merged}`)").result()
        n_tickers = client.query(f"select count(*) as n_tickers from `{table_id_merged}` where date='{day}'").to_dataframe()['n_tickers'].tolist()[0]
        assert n_tickers>3000,'MOVING TO CSV'
    except:
        try:
            ch_table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks_csv"
            client.query(f"drop table if exists `{table_id_merged}`;create table `{table_id_merged}` as (select * from `{ch_table_id_merged}`)").result()
            n_tickers = client.query(f"select count(*) as n_tickers from `{table_id_merged}` where date='{day}'").to_dataframe()['n_tickers'].tolist()[0]
            assert n_tickers>3000,'MOVING TO IB'
        except:
            ch_table_id_merged = f"finityx-app.checked_data.{day}_open_merged_stocks_ib"
            client.query(f"drop table if exists `{table_id_merged}`;create table `{table_id_merged}` as (select * from `{ch_table_id_merged}`)").result()
            n_tickers = client.query(f"select count(*) as n_tickers from `{table_id_merged}` where date='{day}'").to_dataframe()['n_tickers'].tolist()[0]
            assert n_tickers>3000,'DATA MISSING'

    # remove_small_stocks_qstr = f"""
    # select * from
    # (
    # select a.*,b.rel_price_open from `{table_id_merged}` as a
    # join
    # (
    # select ticker,last_value(yahoo_unadjusted_open) over (partition  by ticker order by  date)  as rel_price_open from
    # (select * from `{table_id_merged}` where yahoo_unadjusted_open is not null)
    # where date = '{day}'
    # ) as b
    # on a.ticker = b.ticker
    # )
    # where rel_price_open>{MIN_STOCK_PRICE}
    # """
    #
    # client.query(f"create or replace table `{table_id_merged}` as ({remove_small_stocks_qstr})").result()

def monitor_check(table_id_merged):
    missing = 'stocks.missing_tickers_today'
    important = 'stocks.important_tickers'
    fishy = 'stocks.fishy_tickers'
    mapping = 'starmine.all_companies_ultimate_mapping'
    client.query(f"create or replace view {missing} as (select distinct a.ticker, CURRENT_TIMESTAMP() "
                 f"as update_datetime from `{mapping}` a left join `{table_id_merged}` b on a.ticker=b.ticker where b.ticker is null)").result()
    client.query(
        f"""create or replace view `checked_data.check_monitor` as (select count(*) as count, 'ticker_index_null' as check_type, 
        CURRENT_TIMESTAMP() as update_datetime from `{table_id_merged}` where ticker_index is null
        union all select count(*) as count, 'missing_fishy_tickers' as check_type, CURRENT_TIMESTAMP() as update_datetime from `{missing}` a join {fishy} b on a.ticker=b.ticker
        union all select count(*) as count, 'missing_important_tickers' as check_type, CURRENT_TIMESTAMP() as update_datetime from `{missing}` a join {important} b on a.ticker=b.ticker)""").result()


def intraday_trigger(table, start_date, end_date):
    publisher = pubsub_v1.PublisherClient()
    project_id = 'finityx-app'
    topic_path = publisher.topic_path(project_id, 'intraday')
    tickers_list = get_tickers_list()
    print(start_date)
    print(end_date)
    function_trigger('intraday_finnhub_upload', {'start_date': str(start_date), 'end_date': str(end_date),
                                                 'table_name': table}, tickers_list, publisher, topic_path, ntickers=1)



print("***********************open *********************")
print("2022-04-28 v8")
print(os.getenv('open'))
print("*************************************************")

publisher = pubsub_v1.PublisherClient()
project_id = 'finityx-app'
topic_path = publisher.topic_path(project_id, 'data_generation')
start_date = datetime.strptime('2000-01-01', '%Y-%m-%d').date()
end_date = datetime.strptime('2016-04-25', '%Y-%m-%d').date()
start = end_date - timedelta(days=100)

if os.getenv('open') == '0':
    trigger_daily()

if os.getenv('open') == '1':
    pre_open(get_tickers_list(), 'insert_open_yahoo', publisher, topic_path, '_yahoo')

# if os.getenv('open') == '2':
#     while start > start_date:
#         intraday_trigger('intraday_finnhub_final', start, end_date)
#         end_date = start
#         start = end_date - timedelta(days=100)

if os.getenv('open') == '2':
    create_ib_table(get_tickers_list(),'_ib')

if os.getenv('open') == '3':
    pre_open(get_tickers_list(), 'insert_by_csv', publisher, topic_path, '_csv')

if os.getenv('open') == '4':
    pre_open(get_tickers_list(), 'insert_fast', publisher, topic_path, '_fast')

if os.getenv('open') == '5':
    create_polygon_table(get_tickers_list(),'_polygon')


if os.getenv('open') == 'beta':
    beta_calc(f"checked_data.{day}_merged_stocks")

if os.getenv('open') == 'choose_data_table':
    choose_data_table_for_open(suffix='')

if os.getenv('open') == 'late_close':
    download_late_close()

