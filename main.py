import json,base64
from upload_ticker import upload_ticker
from create_merged_data import create_merged_table
from checked_table import run
from daily_run_main import fill_missing_rows
from intraday_upload import intraday_finnhub_upload, intraday_eodhistorical_upload
from daily_run_main import insert_open_yahoo
from daily_run_main import insert_by_csv
from daily_run_main import insert_fast

def main(event, context):
    js = json.loads(base64.b64decode(event['data']).decode('utf-8'))
    print(js['function_name'], js['input_function'])
    eval(js['function_name'])(**js['input_function'])