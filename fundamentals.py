import math
import re
import string
import time
from datetime import date
import pandas_gbq
from google.cloud import bigquery
from pandas import DataFrame
import pandas as pd

client = bigquery.Client()


class Fundamentals:
    keys_table_path = 'fundamentals.full_keys'
    day = date.today()
    suppliers_tables_keys_map = {'eikon':{'fundamentals.eikon_fundamentals_full': ['ticker', 'date']},
                      'finnhub':{'fundamentals.for_yarden2': ['symbol', 'data_endDate']},
                      'modeling_prep':{'fundamentals.modeling_prep_fundamentals_full': ['symbol', 'date']}}
    merged_table_path = f'fundamentals.merged_fundamentals_{day}'
    checked_table_path = f'fundamentals.checked_fundamentals_{day}'
    mapping_table_path = 'fundamentals.fundamentals_parameters_mapping'
    keys = [list(sup.values())[0] for sup in list(suppliers_tables_keys_map.values())]

    def create_full_keys_table(self, table_path_dest):
        script = []
        for i, key in enumerate(self.keys):
            script.append(f"""select {','.join(key)} from {list(list(self.suppliers_tables_keys_map.values())[i].keys())[0]} group by {','.join(key)}""")
        df = client.query(' union all '.join(script)).result().to_dataframe()
        pandas_gbq.to_gbq(df, table_path_dest, project_id='finityx-app', if_exists="replace")

    def get_fields(self, mapping_table_path):
        fields_res = client.query(f"select distinct field from {mapping_table_path}").result()
        return [field for field in fields_res]

    def get_suppliers_field_list(self, field):
        supplier_field = client.query(
            f"select supplier,supplier_field,field from {self.mapping_table_path} where field = '{field}' order by supplier").result()
        return [field for field in supplier_field]

    def get_checked_field(self, supplier_field_list, merged_df, min_diff_percent):
        checked = []
        data_list = list(merged_df.values)
        for data in data_list:
            done = False
            field_values = data[len(self.keys[0]):]
            for i, supplier in enumerate(field_values):
                if re.match(r'^-?\d+(?:\.\d+)?$', str(supplier)) is None or done == True or supplier is None:
                    break
                if math.isnan(float(supplier)):
                    break
                for j, supplier2 in enumerate(field_values[i:]):
                    if re.match(r'^-?\d+(?:\.\d+)?$', str(supplier2)) is None or supplier2 is None:
                        break
                    if not math.isnan(float(supplier2)) and float(supplier) != 0:
                        if abs(1 - float(supplier2) / float(supplier)) < min_diff_percent:
                            checked_row = list(data[:len(self.keys[0])]) + [supplier2]
                            checked.append(checked_row)
                            done = True
                            break
        return DataFrame(checked, columns=['ticker', 'date', supplier_field_list[0][2]])

    def merge_df_list(self, df_list, key, how):
        if len(df_list) > 1:
            d = pd.merge(df_list[0], df_list[1], how=how, on=key)
            for df in df_list[1:]:
                d = pd.merge(d, df, how=how, on=key)
            df_list = [d]
        return df_list[0]

    def get_merged_suppliers_df_by_field(self, suppliers_field_list):
        fields_list, suppliers_alias = [],{}
        suppliers_tables = list(string.ascii_lowercase)[1:]
        for i, s in enumerate(suppliers_field_list):
            suppliers_alias[s[0]] = suppliers_tables[i]
            fields_list.append(f"{suppliers_tables[i]}.{s[1]} as {s[2]}_{s[0]}")
        script = f"""select a.ticker,a.date, {','.join(fields_list)} from {self.keys_table_path} a """
        for supplier in list(suppliers_alias.keys()):
            join_condition = []
            for j, k in enumerate(list(self.suppliers_tables_keys_map[supplier].values())[0]):
                if "date" in k or 'Date' in k:
                    join_condition.append(f"safe_cast(safe_cast({suppliers_alias[supplier]}.{k} as timestamp) as date)"
                                          f" = safe_cast(safe_cast(a.{self.keys[0][j]} as timestamp) as date) ")
                else:
                    join_condition.append(f"{suppliers_alias[supplier]}.{k} = a.{self.keys[0][j]} ")
            script += f"full outer join `{list(self.suppliers_tables_keys_map[supplier].keys())[0]}` as {suppliers_alias[supplier]} on {' and '.join(join_condition)}"
        return client.query(script).result().to_dataframe()

    def run(self):
        self.create_full_keys_table(self.keys_table_path)
        merged_suppliers, checked_fields = [], []
        fields = self.get_fields(self.mapping_table_path)
        for field in fields:
            suppliers_field_list = self.get_suppliers_field_list(field[0])
            merged_suppliers_df = self.get_merged_suppliers_df_by_field(suppliers_field_list)
            merged_suppliers.append(merged_suppliers_df)
            checked_fields.append(self.get_checked_field(suppliers_field_list, merged_suppliers_df, 0.03))
        merged_suppliers_df = self.merge_df_list(merged_suppliers, self.keys[0], 'outer')
        checked_fields_df = self.merge_df_list(checked_fields, self.keys[0], 'outer')
        pandas_gbq.to_gbq(merged_suppliers_df, self.merged_table_path, project_id='finityx-app', if_exists='replace')
        pandas_gbq.to_gbq(checked_fields_df, self.checked_table_path, project_id='finityx-app', if_exists='replace')


# fund = Fundamentals()
# fund.run()
