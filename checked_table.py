from datetime import date

import pandas as pd
from google.cloud import bigquery
import numpy, pandas_gbq
import json, base64
import scipy.stats


client = bigquery.Client()


def run(day):
    tablename = f'`checked_data.{day}_merged_stocks`'
    out_table = f'`checked_data.{day}_checked_stocks`'

    merged_query = f"""
    select *,
    (alpha_unadjusted_close*alpha_adjustment_factor)/(1e-10+alpha_lag_close) - 1 as alpha_div_close,
    (yahoo_unadjusted_close*yahoo_adjustment_factor)/(1e-10+yahoo_lag_close) - 1 as yahoo_div_close,
    (quandl_unadjusted_close*quandl_adjustment_factor)/(1e-10+quandl_lag_close) - 1 as quandl_div_close,

    (alpha_unadjusted_open*alpha_adjustment_factor)/(1e-10+alpha_lag_open) - 1 as alpha_div_open,
    (yahoo_unadjusted_open*yahoo_adjustment_factor)/(1e-10+yahoo_lag_open) - 1 as yahoo_div_open,
    (quandl_unadjusted_open*quandl_adjustment_factor)/(1e-10+quandl_lag_open) - 1 as quandl_div_open,

    alpha_unadjusted_open/(1e-10+alpha_unadjusted_close) - 1 as alpha_div_open2close,
    yahoo_unadjusted_open/(1e-10+yahoo_unadjusted_close) - 1 as yahoo_div_open2close,
    quandl_unadjusted_open/(1e-10+quandl_unadjusted_close) - 1 as quandl_div_open2close,

    (alpha_unadjusted_open*alpha_adjustment_factor)/(1e-10+alpha_lag_close) - 1 as alpha_div_close2open,
    (yahoo_unadjusted_open*yahoo_adjustment_factor)/(1e-10+yahoo_lag_close) - 1 as yahoo_div_close2open,
    (quandl_unadjusted_open*quandl_adjustment_factor)/(1e-10+quandl_lag_close) - 1 as quandl_div_close2open,

    (alpha_unadjusted_close*alpha_unadjusted_volume)     as alpha_turnover,
    (yahoo_unadjusted_close*yahoo_unadjusted_volume)     as yahoo_turnover,
    (quandl_unadjusted_close*quandl_unadjusted_volume)   as quandl_turnover

    from 
    (select *,
    lag(alpha_unadjusted_close*alpha_adjustment_factor) over(partition by ticker order by date) as alpha_lag_close,
    lag(yahoo_unadjusted_close*yahoo_adjustment_factor) over(partition by ticker order by date) as yahoo_lag_close,
    lag(quandl_unadjusted_close*quandl_adjustment_factor) over(partition by ticker order by date) as quandl_lag_close,

    lag(alpha_unadjusted_open*alpha_adjustment_factor) over(partition by ticker order by date) as alpha_lag_open,
    lag(yahoo_unadjusted_open*yahoo_adjustment_factor) over(partition by ticker order by date) as yahoo_lag_open,
    lag(quandl_unadjusted_open*quandl_adjustment_factor) over(partition by ticker order by date) as quandl_lag_open,

    from  (select distinct(CONCAT(ticker,date))as tdate,* from {tablename}))

    """
    start_q = """
    select ticker, date,alpha_adjustment_factor,yahoo_adjustment_factor,quandl_adjustment_factor,
    alpha_unadjusted_high,yahoo_unadjusted_high,quandl_unadjusted_high,
    alpha_unadjusted_low,yahoo_unadjusted_low,quandl_unadjusted_low,
    alpha_unadjusted_close,yahoo_unadjusted_close,quandl_unadjusted_close,
    alpha_unadjusted_open,yahoo_unadjusted_open,quandl_unadjusted_open,
    alpha_unadjusted_volume,yahoo_unadjusted_volume,quandl_unadjusted_volume,
    alpha_turnover,yahoo_turnover,quandl_turnover,
    """

    mid_queries = []
    field_ths = {'div_close': 0.001, 'div_open': 0.001, 'div_close2open': 0.001, 'div_open2close': 0.001}
    for field_name, field_th in field_ths.items():
        mid_query = f"""
        alpha_{field_name},yahoo_{field_name},quandl_{field_name},
        case (abs(alpha_{field_name}-yahoo_{field_name})<={field_th}) WHEN True then 1 else 0 end +
        case (abs(alpha_{field_name}-quandl_{field_name})<={field_th}) WHEN True then 1 else 0 end + 1
        as alpha_{field_name}_counter,

        case (abs(yahoo_{field_name}-alpha_{field_name})<={field_th}) WHEN True then 1 else 0 end +
        case (abs(yahoo_{field_name}-quandl_{field_name})<={field_th}) WHEN True then 1 else 0 end + 1
        as yahoo_{field_name}_counter,

        case (abs(quandl_{field_name}-yahoo_{field_name})<={field_th}) WHEN True then 1 else 0 end +
        case (abs(quandl_{field_name}-alpha_{field_name})<={field_th}) WHEN True then 1 else 0 end + 1
        as quandl_{field_name}_counter
        """
        mid_queries.append(mid_query)

    explicit_counters_queries = start_q + ','.join(mid_queries) + f""" from ({merged_query})"""
    start_q = """select ticker,date,alpha_adjustment_factor,yahoo_adjustment_factor,quandl_adjustment_factor,
    alpha_unadjusted_close,yahoo_unadjusted_close,quandl_unadjusted_close,
    alpha_unadjusted_open,yahoo_unadjusted_open,quandl_unadjusted_open,
    """

    for field_name in field_ths:
        mid_query = f"""
        case Greatest(alpha_{field_name}_counter,yahoo_{field_name}_counter,quandl_{field_name}_counter) 
        when (alpha_{field_name}_counter) then alpha_{field_name}
        when (yahoo_{field_name}_counter) then yahoo_{field_name}
        when (quandl_{field_name}_counter) then quandl_{field_name} end as {field_name},
        Greatest(alpha_{field_name}_counter,yahoo_{field_name}_counter,quandl_{field_name}_counter) as {field_name}_counter,
        if(alpha_{field_name} is NULL,0,1) + if(yahoo_{field_name} is NULL,0,1) + if(quandl_{field_name}  is NULL,0,1)  as exists_{field_name}_counter
        """
        mid_queries.append(mid_query)

    remaining_fields = ['unadjusted_open', 'unadjusted_close', 'unadjusted_high', 'unadjusted_low',
                        'unadjusted_volume', 'adjustment_factor']
    for field_name in remaining_fields:
        mid_query = f"""
        case 
        WHEN alpha_div_close_counter=GREATEST(COALESCE(alpha_div_close_counter,
      yahoo_div_close_counter,
      quandl_div_close_counter), COALESCE(yahoo_div_close_counter,
      alpha_div_close_counter,
      quandl_div_close_counter),  
      COALESCE(quandl_div_close_counter,alpha_div_close_counter,yahoo_div_close_counter)) and alpha_{field_name} is not null THEN alpha_{field_name}
    WHEN yahoo_div_close_counter=GREATEST(COALESCE(alpha_div_close_counter,
      yahoo_div_close_counter,
      quandl_div_close_counter), COALESCE(yahoo_div_close_counter,
      alpha_div_close_counter,
      quandl_div_close_counter),  
      COALESCE(quandl_div_close_counter,alpha_div_close_counter,yahoo_div_close_counter)) and yahoo_{field_name} is not null THEN yahoo_{field_name}
    WHEN quandl_div_close_counter=GREATEST(COALESCE(alpha_div_close_counter,
      yahoo_div_close_counter,
      quandl_div_close_counter), COALESCE(yahoo_div_close_counter,
      alpha_div_close_counter,
      quandl_div_close_counter),  
      COALESCE(quandl_div_close_counter,alpha_div_close_counter,yahoo_div_close_counter)) and quandl_{field_name} is not null THEN quandl_{field_name}
END as {field_name}
        """
        mid_queries.append(mid_query)

    ## field_name = 'turnover'

    mid_query = f"""
    LEAST(alpha_turnover,yahoo_turnover,quandl_turnover) as turnover
    """
    mid_queries.append(mid_query)

    ## field_name = 'highest_unadjusted_for_commission'
    mid_query = f"""
    LEAST(alpha_unadjusted_close,yahoo_unadjusted_close,quandl_unadjusted_close) as lowest_unadjusted_for_commission
    """
    mid_queries.append(mid_query)

    for field_name in ['close', 'open', 'open2close', 'close2open']:
        mid_query = f"""
        (Greatest(alpha_div_{field_name},yahoo_div_{field_name},quandl_div_{field_name}) - LEAST(alpha_div_{field_name},yahoo_div_{field_name},quandl_div_{field_name}) ) as {field_name}_error
        """
        mid_queries.append(mid_query)

    query = start_q + ','.join(mid_queries) + f""" from ({explicit_counters_queries})"""
    query = f"""select *, {' and '.join([f'exists_{field_name}_counter={field_name}_counter and {field_name}_counter>1' for field_name in field_ths])} as tradeable 
    from ({query})"""

    tot_query = f"""select a.ticker_index ,b.* from (select *,row_number() over() as ticker_index from (select distinct(ticker)  from ({query}))) as a join ({query})  as b on a.ticker = b.ticker"""
    client.query(f"""create table {out_table}
    PARTITION BY
    RANGE_BUCKET(ticker_index, GENERATE_ARRAY(0, 5000, 2))
    as ({tot_query})""").result()