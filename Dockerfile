FROM python:3.7


WORKDIR /opt/model
ENV PYTHONPATH=/opt/model

COPY . .
RUN apt-get -y update && apt-get install -y git libpq-dev \
 && pip install -r requirements.txt \
 && chmod +x ./run.sh

CMD [ "./run.sh"]