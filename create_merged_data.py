import pytz
from datetime import date, timedelta, datetime

from google.cloud import bigquery

from checked_table import run

client = bigquery.Client()


def set_table_expiration(table_name: str, daily_schema: str = None, days: int = 7):
    try:
        if daily_schema is None:
            parts = table_name.split('.')
            if len(parts) is 2:
                daily_schema = parts[0]
                table_name = parts[1]
            if len(parts) is 3:
                daily_schema = parts[1]
                table_name = parts[2]
        project = client.project
        dataset_ref = bigquery.DatasetReference(project, daily_schema)
        table_ref = dataset_ref.table(table_name)
        table = client.get_table(table_ref)  # API request

        print(f'Working on table {daily_schema}.{table_name}')

        if table.expires is not None:
            print(f'Table {daily_schema}.{table_name} already has expiration: {table.expires}')
            return

        expiration = datetime.now(pytz.utc) + timedelta(days=days)
        table.expires = expiration
        table = client.update_table(table, ["expires"])
        print(f'Table {daily_schema}.{table_name} expiration changed: {table.expires}')
    except Exception as e:
        print(f'Could not change table {daily_schema}.{table_name} expiration. Error: {str(e)}')


def build_yahoo_table(table_name, day):
    client.query(f"create or replace table `{table_name}` as (select distinct * from `{table_name}`)").result()
    script = f"""  
    create or replace table stocks.yahoo_temp_calculation_table as  
    ( 
    SELECT ticker,market_date,StockSplits,High, Low,Open, Close, Volume, AdjClose, 
    sum(ln( 
    CASE WHEN StockSplits = 0 THEN 1 
         else StockSplits 
    END 
    )) 
    over(partition by ticker order by market_date DESC) as log_cumprod_stock_splits from `{table_name}` 
    ) 
    """
    client.query(script).result()
    dest_table_name = f'stocks.{day}_yahoo_uniform'
    script = f""" 
    create or replace table `{dest_table_name}` as ( 
    select 
    ticker,market_date, High * exp(log_cumprod_stock_splits) as high, Low * exp(log_cumprod_stock_splits) as low, 
    Close * exp(log_cumprod_stock_splits) as close, Open * exp(log_cumprod_stock_splits) as open,  AdjClose / (close*exp(log_cumprod_stock_splits)) as adjustment_factor, Volume as volume 
    from 
    stocks.yahoo_temp_calculation_table where close!=0 
    ) 
    """
    client.query(script).result()
    set_table_expiration(table_name)
    set_table_expiration(dest_table_name)
    return dest_table_name


def build_quandl_table(table_name, day):
    client.query(f"create or replace table `{table_name}` as (select distinct * from `{table_name}`)").result()
    dest_table_name = f'stocks.{day}_quandl_uniform'
    script = f""" 
    create or replace table `{dest_table_name}` as ( 
    select 
    ticker, market_date, high, low, close, open, adj_close / close as adjustment_factor, volume 
    from 
    `{table_name}` where close != 0 
    ) 
    """
    client.query(script).result()
    client.query(f"create or replace table `{dest_table_name}` as (select distinct * from `{dest_table_name}`)").result()
    set_table_expiration(dest_table_name)
    return dest_table_name


# def build_intrinio_table(table_name, day):
#
#     dest_table_name = f'stocks.{day}_intrinio_uniform'
#     script = f"""
#     create or replace table `{dest_table_name}` as (
#     select
#     ticker,cast(date as TIMESTAMP) as market_date, high as high, low  as low,
#     close as close, open  as open, adj_close / close as adjustment_factor, Volume as volume
#     from
#     `{table_name}`
#     )
#     """
#     client.query(script).result()
#     set_table_expiration(table_name)
#     set_table_expiration(dest_table_name)
#     return dest_table_name


def build_alpha_table(table_name, day):
    client.query(f"create or replace table `{table_name}` as (select distinct * from `{table_name}`)").result()
    dest_table_name = f'stocks.{day}_alpha_uniform'
    script = f""" 
    create or replace table `{dest_table_name}` as ( 
    select ticker,market_date,high, low, open, close, adjuste/close as  
    adjustment_factor, volume from `{table_name}`  
    where close!=0  
    ) 
    """
    client.query(script).result()
    client.query(
        f"create or replace table `{dest_table_name}` as (select distinct * from `{dest_table_name}`)").result()
    set_table_expiration(table_name)
    set_table_expiration(dest_table_name)
    return dest_table_name


def build_xignite_table(table_name, day):
    client.query(f"create or replace table {table_name} as (select distinct * from {table_name})").result()
    dest_table_name = f'stocks.{day}_xignite_uniform'
    script = f""" 
    create or replace table `{dest_table_name}` as ( 
    select SecuritySymbol as ticker,cast(HistoricalQuotesDate as Timestamp) as market_date, HistoricalQuotesHigh*HistoricalQuotesCumulativeAdjustmentFactor as high, 
    HistoricalQuotesLow*HistoricalQuotesCumulativeAdjustmentFactor as low, HistoricalQuotesClose*HistoricalQuotesCumulativeAdjustmentFactor as close,  
    HistoricalQuotesOpen*HistoricalQuotesCumulativeAdjustmentFactor as open,  
    1/HistoricalQuotesCumulativeAdjustmentFactor as adjustment_factor, HistoricalQuotesVolume as volume from `{table_name}` where HistoricalQuotesDate!="nan" 
    ) 
    """
    client.query(script).result()
    client.query(
        f"create or replace table `{dest_table_name}` as (select distinct * from `{dest_table_name}`)").result()
    set_table_expiration(table_name)
    set_table_expiration(dest_table_name)
    return dest_table_name


def merge_tables(uniform_tables, table_name):
    def generate_data_script(source):
        script = ','.join(
            [f'{source}.{key} as {source}_unadjusted_{key}' for key in ['close', 'open', 'high', 'low', 'volume']])
        script += f',{source}.adjustment_factor as {source}_adjustment_factor '
        return script

    main_source = 'alpha'
    select_script = f"""select case when alpha.ticker is NOT null then alpha.ticker when yahoo.ticker is not null then yahoo.ticker else quandl.ticker end as ticker,  
    case when alpha.market_date is not null then alpha.market_date when yahoo.market_date is not null then yahoo.market_date 
else quandl.market_date end as date,""" + generate_data_script(
        main_source)
    join_script = ''
    for source, table in uniform_tables.items():
        if source != main_source:
            on_clause = f"{main_source}.market_date = {source}.market_date and {main_source}.ticker = {source}.ticker"
            if source == 'quandl':
                on_clause = f"ifnull({main_source}.market_date, ifnull(yahoo.market_date,null)) = {source}.market_date and ifnull({main_source}.ticker, ifnull(yahoo.ticker,null)) = {source}.ticker"
            select_script += ',' + generate_data_script(source)
            join_script += f' full outer join `{uniform_tables[source]}` as {source} ON {on_clause}'

    script = select_script + f' from `{uniform_tables[main_source]}` as {main_source}' + join_script
    client.query(
        f'create or replace table stocks.ticker_indexes1 as (select ticker, row_number() over() as ticker_index from (select distinct ticker from ({script})))').result()
    script = select_script + f' from `{uniform_tables[main_source]}` as {main_source}' + join_script
    client.query(
        f'create or replace table `{table_name}` partition by range_bucket(ticker_index, GENERATE_ARRAY(0, 7000, 5))  as (select distinct a.*, '
        f'indexes.ticker_index as ticker_index from ({script}) a full outer join stocks.ticker_indexes1 as indexes ON a.ticker = indexes.ticker) ').result()
    set_table_expiration(table_name)


def create_merged_table(day):
    daily_schema = 'stocks'
    uniform_tables = {'yahoo': build_yahoo_table(f'{daily_schema}.{day}yahoo', day),
                      'quandl': build_quandl_table(f'{daily_schema}.{day}quandl', day),
                      'alpha': build_alpha_table(f'{daily_schema}.{day}alpha_adjusted', day)}
    merge_tables(uniform_tables, f'checked_data.{day}_merged_stocks')
