import io
import math

import pandas as pd
from google.cloud import bigquery
import pandas_gbq
from datetime import timezone, datetime, timedelta, date
import requests
import pandas
import time

day = date.today()
client = bigquery.Client()
eodhistorical_table = f'intraday.intraday_eodhistorical_{day}'

def intraday_finnhub_upload(start_date, end_date,table_name, tickers_list):
    table_id = client.get_table(f'finityx-app.intraday.{table_name}')
    for tick in tickers_list:
        api_token = 'buts6tf48v6skju2b9ig'
        tick_data=[]
        t=time.time()
        start_unix = int(datetime.strptime(start_date,'%Y-%m-%d').replace(tzinfo=timezone.utc).timestamp())
        end_unix = int(datetime.strptime(end_date,'%Y-%m-%d').replace(tzinfo=timezone.utc).timestamp())
        while end_unix>start_unix:
            try:
                r = requests.get( f'https://finnhub.io/api/v1/stock/candle?symbol={tick}&resolution=1&from={start_unix}&to={end_unix}&token={api_token}',
                                  timeout=3000)
                tick_data=r.json()
                if not tick_data['s'] == 'ok':
                    break
                keys_in = ['c', 'h', 'l', 'o', 't', 'v']
                keys_out = ['close', 'high', 'low', 'open', 'time', 'volume']
                tick_data = {keys_out[i]: tick_data[k] for i, k in enumerate(keys_in)}
                tick_data=pandas.DataFrame.from_dict(tick_data)
                end_unix=tick_data['time'][0]-1
                tick_data['time']  = pandas.to_datetime(tick_data['time'],unit='s')
                tick_data['ticker'] = tick
                tick_data = list(tick_data.values)
                print(f"good fetch for {tick}")
                time.sleep(12)
            except Exception as e:
                print('one time', tick, e)
                time.sleep(12)
            try:
                if tick_data:
                    for index, i in enumerate(list(range(0, math.floor((len(tick_data) / 9999) +1)))):
                        if index == math.floor(len(tick_data) / 9999):
                            client.insert_rows(table_id, tick_data[i * 9999:])
                            print(f"finish upload {tick} {i * 9999}")
                        else:
                            client.insert_rows(table_id, tick_data[i * 9999:(i + 1) * 9999])
                            print(f"finish upload {tick} {i * 9999}")
                else:
                    print(f"{tick} missing data")
            except Exception as e:
                print(f"cant upload {tick}")
                print(e)


def intraday_eodhistorical_upload(start_date, end_date, tickers_list):
    start_unix_end = int(datetime.strptime(start_date, '%Y-%m-%d').replace(tzinfo=timezone.utc).timestamp())
    data = []
    token = '5fba6d0b3a1442.94796703'
    for ticker in tickers_list:
        ticker2 = ticker.replace('.', '-')
        count = 0
        start = datetime.strptime(end_date, '%Y-%m-%d') - timedelta(days=100)
        end = datetime.strptime(end_date, '%Y-%m-%d')
        start_unix = int(start.replace(tzinfo=timezone.utc).timestamp())
        while start_unix_end<start_unix and count<2:
            end_unix = int(end.replace(tzinfo=timezone.utc).timestamp())
            start_unix = int(start.replace(tzinfo=timezone.utc).timestamp())
            try:
                r = requests.get(
                    f'https://eodhistoricaldata.com/api/intraday/{ticker2}.US?api_token={token}&interval=1m&from={start_unix}&to={end_unix}',
                    timeout=3000).content
            except Exception as e:
                print(e)
                break
            df = pd.read_csv(io.StringIO(r.decode('utf-8')))
            if df.columns[0] == 'Value':
                print(f"there is no data for {ticker} from {start}")
                count +=1
            else:
                df['ticker'] = ticker
                data.append(df)
                print(f"finish {ticker} from {start}")
            end = start
            start = end - timedelta(days=100)
    tick_data_all = pandas.concat(data)
    pandas_gbq.to_gbq(tick_data_all, eodhistorical_table, project_id='finityx-app', if_exists='append')
    print("finish upload")


